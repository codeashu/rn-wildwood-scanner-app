import React from 'react';

export async function getOffersByUser(token, id) {
    const response = await fetch('https://offerbotapp.herokuapp.com/api/users/' + id + '/offers', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
    })
    const responseData = await response.json();
    return responseData;
}

export async function updateOfferAsRedeemed(token, offer_id) {
    const response = await fetch('https://offerbotapp.herokuapp.com/api/Offers/updateredeem/' + offer_id, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
    })
    const responseData = await response.json();
    return responseData;
}
