import React from 'react';

export async function login(email , password) {
    const response = await fetch('https://offerbotapp.herokuapp.com/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            email: email,
            password: password,
        })
    })
    const responseData = await response.json();
    return responseData;
}
