import React from 'react';
import { Alert, TextInput, View, StyleSheet, AsyncStorage } from 'react-native';
import { Container, Header, Content, Form, Item, Input, Label, Button, Text, Left, Body, Right, Icon, Title, Card, CardItem } from 'native-base';
import { Constants, BarCodeScanner, Permissions, Notifications } from 'expo';
import { getOffersByUser } from "../services/QR";
import { StackNavigator } from 'react-navigation';


export default class OpenCamera extends React.Component {

  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);

    if (this.props.navigation.state.params.offer_code != null) {
      this.state = {
        id: this.props.navigation.state.params.offer_code
      }
    } else {
      this.state = {
        id: null
      }
    }

  }

  _openCameraFullScreen = () => {
    this.props.navigation.navigate('Qrreader');
  }

  _saveCodeAndContinue = () => {
    this.props.navigation.navigate('Details', { offer_code: this.state.id });
  }

  render() {
    return (
      <Container style={styles.container}>
        <Header style={styles.header}>
          <Left>
            <Button transparent onPress={this._openCameraFullScreen}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={styles.header_title}>QR Scanner Settings</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <Form style={styles.form}>
            <Item stackedLabel style={styles.input_item}>
              <Label style={styles.input_labels}>Enter Offer Code Manually</Label>
              <Input
                style={styles.input}
                onChangeText={(id) => this.setState({ id })}
                value={this.state.id} />
            </Item>
            <Button block dark style={{ margin: 15 }}>
              <Text onPress={this._saveCodeAndContinue}>Save & Continue</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#0B9496',
  },
  header: {
    height: 100,
    backgroundColor: '#000'
  },
  header_title: {
    color: '#fff'
  },
  input: {
    color: '#fff'
  },
  input_labels: {
    color: '#fff',
    fontWeight: 'bold'
  },
  input_item: {
    marginRight: 15
  },
  form: {
    marginTop: 50
  },
});