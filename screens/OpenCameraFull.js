import React from 'react';
import { Alert, Text, Button, TextInput, View, StyleSheet, AsyncStorage, Dimensions, TouchableHighlight, Image } from 'react-native';
import { Constants, BarCodeScanner, Permissions, Notifications, Camera } from 'expo';
import { getOffersByUser } from "../services/QR";
import { StackNavigator, NavigationActions } from 'react-navigation';
import RNFlash from 'react-native-flash';
import MaterialCommunityIcons from '@expo/vector-icons/MaterialCommunityIcons'
import focus from '../assets/images/unrecog.png';
import recognize from '../assets/images/recog.png';

export default class OpenCameraFull extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      hasCameraPermission: null,
      id: null,
      flashmode: 'off',
      cameratype: Camera.Constants.Type.back,
      flashicon: 'flash',
      scan: 'off'
    };
  }

  _requestCameraPermission = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({
      hasCameraPermission: status === 'granted',
    });
  };

  _handleBarCodeRead = ({ type, data }) => {
    let { id, scan } = this.state;
    if (scan === 'on') {
      this.setState({ id: data, scan: 'off' }, () => {
        this._saveCodeAndContinue();
      });
    }
  };

  _saveCodeAndContinue = () => {
    this.setState({ flashmode: 'off', flashicon: 'flash-off' });
    this.props.navigation.navigate('Details', { offer_code: this.state.id });
  }

  _showManualEntry = () => {
    this.props.navigation.navigate('OpenCamera', { offer_code: this.state.id });
  }

  _signOut = () => {
    this.props.navigation.navigate('Login');
  }

  _flashLight = () => {
    if (this.state.flashmode == 'on') {
      this.setState({ flashmode: 'off', flashicon: 'flash-off' })
    } else {
      this.setState({ flashmode: 'on', flashicon: 'flash' })
    }
  }

  _toggleScan = () => {
    let status = this.state.scan === 'on' ? 'off' : 'on';
    this.setState({ scan: status })
  }

  componentDidMount() {
    this._requestCameraPermission();
  }

  render() {
    return (
      <View style={styles.container}>
        {this.state.hasCameraPermission === null ?
          <Text>Requesting for camera permission</Text> :
          this.state.hasCameraPermission === false ?
            <Text>Camera permission is not granted</Text> :
            <BarCodeScanner
              onBarCodeRead={this._handleBarCodeRead.bind(this)}
              style={styles.qrscanner}
              torchMode={this.state.flashmode}>
              <View style={styles.topContainer}>
                <View style={styles.topLeft} />
                <TouchableHighlight onPress={this._flashLight} underlayColor='transparent' style={styles.topRight}>
                  <MaterialCommunityIcons name={this.state.flashicon} size={35} color="#FFFFFF" />
                </TouchableHighlight>
              </View>
              {this.state.scan == 'off' ? <Image source={focus} style={{ height: 200, width: 200 }} /> : <Image source={recognize} style={{ height: 200, width: 200 }} />}
              <View style={styles.botton}>
                <TouchableHighlight style={styles.bottomLeft} onPress={this._showManualEntry}><MaterialCommunityIcons name='settings' size={30} color="#FFFFFF" /></TouchableHighlight>
                <TouchableHighlight style={styles.bottmoCenter} onPress={this._toggleScan}><MaterialCommunityIcons name='image-filter-center-focus-weak' size={30} color="#FFFFFF" /></TouchableHighlight>
                <TouchableHighlight style={styles.bottomRight} onPress={this._signOut}><MaterialCommunityIcons name='exit-to-app' size={30} color="#FFFFFF" /></TouchableHighlight>
              </View>
            </BarCodeScanner>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1',
  },
  qrscanner: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  topContainer: {
    flex: 1,
    flexDirection: 'row',
    position: 'absolute',
    top: 0,
    width: '100%',
    height: 80,
    backgroundColor: 'transparent'
  },
  topLeft: {
    flex: 0.9,
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },
  topRight: {
    marginRight: 30,
    marginTop: 20,
    flex: 0.1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },
  focus: {

  },
  botton: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: 90,
    backgroundColor: 'transparent'
  },
  bottomLeft: {
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#888888',
    height: 50,
    width: 50,
    borderRadius: 25
  },
  bottmoCenter: {
    marginBottom: 20,
    marginRight: 20,
    marginLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
    height: 50,
    width: 50,
    borderRadius: 25
  },
  bottomRight: {
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#888888',
    height: 50,
    width: 50,
    borderRadius: 25
  }



});