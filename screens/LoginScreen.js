import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
  TextInput,
  Alert,
  AsyncStorage,
  StatusBar
} from 'react-native';
import { Constants, BarCodeScanner, Permissions, Notifications } from 'expo';
import { Container, Header, Content, Form, Item, Input, Label, Button, Text, Left, Body, Right, Icon, Title, Card, CardItem, Spinner } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { StackNavigator } from 'react-navigation';
import { login } from "../services/Login";

export default class LoginScreen extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      isError: false,
      errorContent: '',
      isRunning:false
    };
  }

  _login = async () => {
    this.setState({isRunning:true})
    const { email, password } = this.state;

    const clearItems = ['token', 'offer_code'];
    AsyncStorage.multiRemove(clearItems);

    try {
      const responseData = await login(email, password);
      const accessToken = responseData.token;

      if ((accessToken != 'undefined') && (accessToken != null)) {
        AsyncStorage.setItem('token', accessToken);
        this.props.navigation.navigate('Qrreader');
      } else {
        this.setState({
          isError: true,
          isRunning:false,
          errorContent: responseData.description
        })
      }

    } catch (e) {
      this.setState({
        isError: true,
        isRunning:false,
        errorContent: e
      })
    }
  }

  render() {
    const { iserror, errorContent, isRunning } = this.state
    return (
      <Container style={styles.container}>
        <Header style={styles.header}>
          <Left />
          <Body>
            <Title style={styles.header_title}>QR Scanner</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          {!this.state.isRunning?<Form style={styles.login_form}>
            {this.state.isError ?
              <Card style={styles.error_card}>
                <CardItem style={styles.error_card_item}>
                  <Body>
                    <Text style={styles.error_card_item_text}>{this.state.errorContent}}</Text>
                  </Body>
                </CardItem>
              </Card>
              : null}
            <Item stackedLabel style={styles.input_item}>
              <Label style={styles.input_labels}>Username</Label>
              <Input autoCapitalize={'none'} style={styles.input} onChangeText={(text) => this.setState({ email: text })} value={this.state.email} />
            </Item>
            <Item stackedLabel style={styles.input_item}>
              <Label style={styles.input_labels}>Password</Label>
              <Input secureTextEntry={true} style={styles.input} onChangeText={(text) => this.setState({ password: text })} value={this.state.password} />
            </Item>
            <Button block dark style={{ margin: 15 }}>
              <Text onPress={this._login}>Login</Text>
            </Button>
          </Form>:
              <View style={[styles.center,{flexDirection:'row'}]}>
                <Spinner color='white' />
              </View>
          }
        </Content>
      </Container>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#0B9496',
  },
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  errors: {
    color: 'red',
    width: 200,
    height: 44,
    padding: 10,
    marginBottom: 10,
  },
  header: {
    height: 100, 
    backgroundColor: '#000'
  },
  header_title: {
    color: '#fff'
  },
  login_form: {
    marginTop: 50
  },
  input: {
    color: '#fff'
  },
  input_labels: {
    color: '#fff',
    fontWeight: 'bold'
  },
  input_item: {
    marginRight: 15
  },
  error_card: {
    marginLeft: 10, 
    marginRight: 10, 
    marginBottom: 20
  },
  error_card_item: {
    backgroundColor: '#eb4336'
  },
  error_card_item_text: {
    color: '#fff'
  }
});
