import React from 'react';
import { Alert, TextInput, View, StyleSheet, TouchableOpacity, AsyncStorage, Modal, Image } from 'react-native';
import { Constants, BarCodeScanner, Permissions, Notifications } from 'expo';
import { Container, Header, Content, Form, Item, Input, Label, Button, Text, Left, Body, Right, Icon, Title, Card, CardItem, Thumbnail } from 'native-base';
import { StackNavigator } from 'react-navigation';
import { getOffersByUser, updateOfferAsRedeemed } from '../services/QR';
import Toast, { DURATION } from 'react-native-easy-toast'

export default class DetailsScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    this.state = {
      data: [],
      showModal: false,
      modalData: {},
      offer: '',
      offer_code: this.props.navigation.state.params.offer_code || null
    }

    this._toggleModal = this._toggleModal.bind(this);
  }

  async getData() {
    const token = await AsyncStorage.getItem('token');
    const offer_code = this.state.offer_code;
    const responseData = await getOffersByUser(token, offer_code);

    try {
      if (responseData !== null) {
        let saveData = responseData.filter(offer => offer.redeemed == false);
        this.setState({ data: saveData });
      }
    } catch (e) {
      console.log('Error obtaining offer data : ' + e);
    }
  }


  _showRedeemModal(data) {
    this.setState({ modalData: data });
    this._toggleModal();
  }

  _toggleModal() {
    this.setState({
      showModal: !this.state.showModal
    })
  }

  async _redeemOffer(offer_id) {
    const token = await AsyncStorage.getItem('token');
    const success = await updateOfferAsRedeemed(token, offer_id);
    if (success.result) {
      this.getData();
      this._toggleModal();
      this.refs.toast.show('Offer successfully redeemed!');
    } else {
      console.log('Error redeeming offer');
      this.refs.toast.show('Error redeeming offer, please try again!');
    }
  }

  _openCameraFullScreen = () => {
    this.props.navigation.navigate('Qrreader');
  }

  componentDidMount() {
    this.getData();
  }

  render() {
    return (
      <Container style={styles.container}>
        <Header style={styles.header}>
          <Left>
            <Button transparent onPress={this._openCameraFullScreen}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={styles.header_title}>QR Scanner Settings</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <Form style={styles.form}>
            {this.state.data && this.state.data.length <= 0 ?
              <Card>
                <CardItem>
                  <Left>
                    <Body>
                      <Text>No results found!</Text>
                      <Text note>This user has no offers saved.</Text>
                    </Body>
                  </Left>
                </CardItem>
              </Card>
              : null}
            {this.state.data.map((data, index) => {
              return (
                <Card key={index}>
                  <CardItem>
                    <Left>
                      <Thumbnail source={{ uri: data.featured_image_url }} />
                      <Body>
                        <Text>{data.name}</Text>
                        <Text note>{data.pricing}</Text>
                      </Body>
                    </Left>
                    <Right>
                      <Button onPress={() => this._redeemOffer(data.id)}>
                        <Text>Redeem</Text>
                      </Button>
                    </Right>
                  </CardItem>
                </Card>
              );
            })}
            <Toast
              ref='toast'
              style={styles.toast_success}
              position='top'
              fadeInDuration={1000}
              fadeOutDuration={1500} />
          </Form>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#0B9496',
  },
  header: {
    height: 100,
    backgroundColor: '#000'
  },
  header_title: {
    color: '#fff'
  },
  input: {
    color: '#fff'
  },
  input_labels: {
    color: '#fff',
    fontWeight: 'bold'
  },
  input_item: {
    marginRight: 15
  },
  form: {
    marginTop: 15,
    marginLeft: 15,
    marginRight: 15
  },
  toast_success: {
    backgroundColor: 'green'
  }
}
);
