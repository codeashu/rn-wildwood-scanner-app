import React from 'react';
import { Platform } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { TabNavigator, TabBarBottom } from 'react-navigation';

import Colors from '../constants/Colors';

import LoginScreen from '../screens/LoginScreen';
import DetailsScreen from '../screens/DetailsScreen';
import OpenCamera from '../screens/OpenCamera';

export default TabNavigator(
  {
    Login: {
      screen: LoginScreen,
      navigationOptions: {
        tabBarVisible: false,
      }
    },
    Details: {
      screen: DetailsScreen,
      navigationOptions: {
        tabBarVisible: false,
      }
    },
    
  },
  {
    tabBarVisible:false,
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    tabBarOptions: {
      style: {
        backgroundColor: '#000'
      }
    },
    animationEnabled: false,
    swipeEnabled: false,
  }
);
